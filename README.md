#SeeStar 
##An open-source underwater time-lapse imaging system

###Background
There are many systems, both software and instrumentation, that have been developed in an attempt to automate quantification of plankton from images. SeeStar is a modular underwater camera system designed to operate autonomously on platforms of opportunity, including piers, mooring cables, ships, and mobile vehicles. Its goals are to leverage off-the-shelf and inexpensive technology to create a widely accessible system.

![SeeStar prototype](https://bitbucket.org/mbari/seestar/raw/master/images/SCPI_Sys_01b.JPG "Prototype modules")

##SeeStar Wiki
Documentation is posted in the [SeeStar Wiki](https://bitbucket.org/mbari/seestar/wiki/).

##Technical Documents


####[Operations Manual](https://bitbucket.org/mbari/seestar/wiki/Operations%20Manual)

* Description of how to set up SeeStar and prepare for deployment.

####[Technical Manuals](https://bitbucket.org/mbari/seestar/wiki/Technical%20Manual)

* These documents will cover the assembly instructions and theory of operation of the individual modules. 
* This manual will contain all of the information necessary to build a complete SeeStar system.


####[Software Manual](https://bitbucket.org/mbari/seestar/wiki/Software%20Manual)

* These documents will describe firmware of the control board, and software to program the camera.
* The intended audience is advanced users who want to customize their system.

Documents are [outlined here](https://bitbucket.org/mbari/seestar/src/master/SeeStarDocList.md?at=master)

##PDF Downloads


##Contact info

Contact either `ckecy` or `haddock` at `mbari{dot}org` or post an issue here.

-----
![CC-BY-SA](http://i.creativecommons.org/l/by-sa/4.0/88x31.png "CC-BY-SA license")

This work is distributed under a [Creative Commons Attribution-ShareAlike 4.0](http://creativecommons.org/licenses/by-sa/4.0) license.
