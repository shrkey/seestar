#### Install `msysgit` from [github](http://msysgit.github.io)

Set it up with your bitbucket info

    git config --global color.ui "auto"
    git config --global user.name "Your Name"
    git config --global user.email "your@email.address"

Use `cd` to move to the folder where you want the repo to be located on your computer.

The menu in bitbucket will give you options:

![Bitbucket Menus](https://bitbucket.org/mbari/seestar/raw/master/images/bitbucketmenu.png "Bitbucket menus")
![Bitbucket Clone Command](https://bitbucket.org/mbari/seestar/raw/master/images/bitbucketclone.png "Bitbucket clone")

### Create ssh keys
Associate them with your account on bitbucket, so you don't have to enter password every time

    ssh-keygen            #accept the default folder and enter a password
    cat ~/.ssh/id_rsa.pub
	
 * Copy the output of that command. Then go to your profile page, or this link
(replacing `yourname` with your username:
    https://bitbucket.org/account/user/yourname/ssh-keys/
	
 * Choose `Add key` and paste your copied key into that window.
 * Remove your e-mail address from the end of the key...

### Clone the repository from the bitbucket server to make a local copy:

    git clone git@bitbucket.org:mbari/seestar.git

(It will create a `seestar` folder for you)

Edit and create documents in this folder using your normal tools.

When done with your edits:

     git fetch origin/master            # To get changes others have pushed
	 git add -A .                       # Add new files
	 git commit -am "Adding new file"   # Give status report, get read to push

	 git pull                           # Before pushing, get other changes 

	 git push                           # May need to do `git push origin master` first time
	 
	 git status                         # Show local differences

Find what is the status of remote machine:
    
    alias grd='git diff --stat origin/master' # see all changes, summarized into statistics

