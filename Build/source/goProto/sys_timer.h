/****************************************************************************/
/* Created 2009 (Anno Domini)                                               */
/****************************************************************************/
/* MBARI SeeStar (See http://www.mbari.org/seestar/) is licensed under a    */
/* Creative Commons Attribution-ShareAlike 4.0 International License.       */
/*                                                                          */
/* You should have received a copy of the license along with this work. If  */
/* not, see <http://creativecommons.org/licenses/by-sa/4.0/>.               */
/*                                                                          */
/* Citation: F. Cazenave, C. Kecy, M. Risi, S.H.D. Haddock (in press)       */
/*    "SeeStar: a low-cost, modular, and open-source camera system          */
/*    for subsea observations", IEEE Oceans 2014                            */
/*                                                                          */
/****************************************************************************/

#ifndef SYS_TIMER_H
#define SYS_TIMER_H

typedef struct
{
    int active;
    unsigned long startTicks;
    unsigned long totalTicks;

} Timer;


/* intialize the timer */
void tmrInit();

/* perform any timer tasks in main loop */
void tmrTask();

/* see if the timer is running */
int tmrIsRunning(Timer* timer);

/* Create the timer */
void tmrCreate(Timer* timer);

/* Clear the timer */
void tmrClear(Timer* timer);

/* Read the total amount of elapsed time the timer has running */
unsigned long tmrRead(Timer* timer);

/* Start the timer */
unsigned long tmrStart(Timer* timer);

/* Stop the timer */
unsigned long tmrStop(Timer* timer);

/* delay for a specified number of timer ticks */
void tmrDelayTicks(unsigned int ticks);

/* delay for a specified number of milliseconds */
void tmrDelayMs(unsigned int ms);

/* get internal tick counts */
unsigned long tmrGetTicks();

#endif


