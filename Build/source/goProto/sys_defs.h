/****************************************************************************/
/* Created 2011 (Anno Domini)                                               */
/****************************************************************************/
/* MBARI SeeStar (See http://www.mbari.org/seestar/) is licensed under a    */
/* Creative Commons Attribution-ShareAlike 4.0 International License.       */
/*                                                                          */
/* You should have received a copy of the license along with this work. If  */
/* not, see <http://creativecommons.org/licenses/by-sa/4.0/>.               */
/*                                                                          */
/* Citation: F. Cazenave, C. Kecy, M. Risi, S.H.D. Haddock (in press)       */
/*    "SeeStar: a low-cost, modular, and open-source camera system          */
/*    for subsea observations", IEEE Oceans 2014                            */
/*                                                                          */
/****************************************************************************/

#ifndef SYS_DEFS_H
#define SYS_DEFS_H
#include <p24fxxxx.h>

/* general defs */
#define TRUE    1
#define FALSE   0

#define HI      1
#define LO      0

#define ON      1
#define OFF     0

/* serial I/O defs */
#define SER_CONSOLE     1
#define SER_SPARE       2

/* timer defs */
#define TMR_FREQ    100

/* ex_rtc I/O defs */
#define EX_RTC_TRIS _TRISB13
#define EX_RTC_CS   _LATB13

/* data_flash I/O defs */
#define DATA_FLASH_TRIS _TRISC9
#define DATA_FLASH_CS   _LATC9

/* configuration parameters */
#define CFG_DEFAULT_BOOT_FLAG       0x0000
#define CFG_ACTIVE_BOOT_FLAG        0xAAAA

#define CFG_AUTO_RUN_MODE_DEF       OFF

#define CFG_AUTO_RUN_DELAY_MIN      0L
#define CFG_AUTO_RUN_DELAY_DEF      0L
#define CFG_AUTO_RUN_DELAY_MAX      86400L

#define CFG_AUTO_RUN_DURATION_MIN   60L  
#define CFG_AUTO_RUN_DURATION_DEF   3600L
#define CFG_AUTO_RUN_DURATION_MAX   31536000L

#define CFG_SAMPLE_MODE_OFF         0
#define CFG_SAMPLE_MODE_PIC         1
#define CFG_SAMPLE_MODE_VID         2
#define CFG_SAMPLE_MODE_HR3         3
#define CFG_SAMPLE_MODE_DEF         CFG_SAMPLE_MODE_OFF

#define CFG_SAMPLE_INTERVAL_MIN     1L
#define CFG_SAMPLE_INTERVAL_DEF     60L
#define CFG_SAMPLE_INTERVAL_MAX     86400L

#define CFG_SAMPLE_DURATION_MIN     0L
#define CFG_SAMPLE_DURATION_DEF     0L
#define CFG_SAMPLE_DURATION_MAX     86400L

#define CFG_LED_ON_DELAY_MIN        0L
#define CFG_LED_ON_DELAY_DEF        0L
#define CFG_LED_ON_DELAY_MAX        3600L
    
#define CFG_LED_ON_DURATION_MIN     0L
#define CFG_LED_ON_DURATION_DEF     0L
#define CFG_LED_ON_DURATION_MAX     86400L

#define CFG_VOLT_STOP_LEVEL_MIN     0L
#define CFG_VOLT_STOP_LEVEL_DEF     0L
#define CFG_VOLT_STOP_LEVEL_MAX     30000L

#define CFG_DEBUG_BITS_DEF          0L

#define HRO_DEBUG_BIT               0x00000001
#define TSK_DEBUG_BIT               0x00000002


/* analog calibration values and defs */

#define ANA_CURRENT 0x0004
#define ANA_VOLTAGE 0x0005

/******************************************************   
*******************************************************   
Measured on 3/20/2014 board #5 of revision #2 
 
Voltage:
     
     Y V =   X cnts
    ---------------
     5 V = 239 cnts
    10 V = 481 cnts
    
    M_VOLT_CAL = (Y2 - Y1) / (X2 - X1)
    
    B_VOLT_CAL = Y2 -  M_VOLT_CAL(X2)

Current:
     
        Y A =   X cnts
    ------------------
    0.039 A =  10 cnts
    0.504 A = 155 cnts
    
    M_CURR_CAL = (Y2 - Y1) / (X2 - X1)
    
    B_CURR_CAL = Y2 -  M_VOLT_CAL(X2)

******************************************************   
******************************************************/

#define M_VOLT_CAL  0.020661157
#define B_VOLT_CAL  0.061983471

#define M_CURR_CAL  0.003206897
#define B_CURR_CAL  0.006931034

#endif
