/****************************************************************************/
/* Created 2012 (Anno Domini)                                               */
/****************************************************************************/
/* MBARI SeeStar (See http://www.mbari.org/seestar/) is licensed under a    */
/* Creative Commons Attribution-ShareAlike 4.0 International License.       */
/*                                                                          */
/* You should have received a copy of the license along with this work. If  */
/* not, see <http://creativecommons.org/licenses/by-sa/4.0/>.               */
/*                                                                          */
/* Citation: F. Cazenave, C. Kecy, M. Risi, S.H.D. Haddock (in press)       */
/*    "SeeStar: a low-cost, modular, and open-source camera system          */
/*    for subsea observations", IEEE Oceans 2014                            */
/*                                                                          */
/****************************************************************************/
/* Routines derived from Glen Akins gptestbrd01.c program                   */
/* (See http://bikerglen.com/ )                                             */
/****************************************************************************/
#include "i2c.h"

#define I2C_DELAY       1 /* 12 us */
#define ACK             1
#define NO_ACK          0

void i2cInit()
{
	WRITE_scl();
	WRITE_sda();
}

void i2cAckPolling(unsigned char device_address)
{
    for(;;)
    {
        i2cStart();
        if (i2cSendByte(device_address) == ACK ) 
            break;
    }

    i2cStop();
}


void i2cStart(void)
{
    WRITE_sda();
    SDA = 1;
    i2cDelay(I2C_DELAY);

    SCL = 1;
    i2cDelay(I2C_DELAY);

    SDA = 0;
    i2cDelay(I2C_DELAY);
}

void i2cStop(void)
{
    SCL = 0;
    i2cDelay(I2C_DELAY);

    WRITE_sda();

    SDA = 0;
    i2cDelay(I2C_DELAY);

    SCL = 1;
    i2cDelay(I2C_DELAY);

    SDA = 1;
    i2cDelay(I2C_DELAY);
}

unsigned char i2cReadByte(void)
{
    int j;
    int in_byte = 0;

    SCL = 0;

    READ_sda();

    for (j = 0; j < 8; j++)
    {
        SCL = 0;
        i2cDelay(I2C_DELAY);

        SCL = 1;
        i2cDelay(I2C_DELAY);

        in_byte = in_byte << 1;
        in_byte |= SDARD;
    }

    return(in_byte);
}

unsigned char i2cSendByte(unsigned char out_byte)
{
    unsigned char i;

    WRITE_sda();

    for ( i = 0 ; i < 8 ; i++ )
    {
        SCL = 0;
        i2cDelay(I2C_DELAY);

        if ( out_byte & 0x80 )
            SDA = 1;
        else
            SDA = 0;

        out_byte = out_byte << 1;
        i2cDelay(0);

        SCL = 1;
        i2cDelay(I2C_DELAY);
    }

    /* Read ACK */
    SCL = 0;
    i2cDelay(I2C_DELAY);

    READ_sda();

    SCL = 1;
    i2cDelay(I2C_DELAY);

    /* Wait for IC to acknowledge */
    for ( i = 0 ; i < 255 ; i++ )
        if ( SDARD == 0 ) break;

    SCL = 0;
    i2cDelay(I2C_DELAY);

    if ( i == 255 ) return(NO_ACK);

    return(ACK);
}

/* delay turns out to be = 8.8us + 3.2us * loops */

void i2cDelay(unsigned char loops)
{
    while ( loops-- )
        Nop ();
}

