/****************************************************************************/
/* Created 2010 (Anno Domini)                                               */
/****************************************************************************/
/* MBARI SeeStar (See http://www.mbari.org/seestar/) is licensed under a    */
/* Creative Commons Attribution-ShareAlike 4.0 International License.       */
/*                                                                          */
/* You should have received a copy of the license along with this work. If  */
/* not, see <http://creativecommons.org/licenses/by-sa/4.0/>.               */
/*                                                                          */
/* Citation: F. Cazenave, C. Kecy, M. Risi, S.H.D. Haddock (in press)       */
/*    "SeeStar: a low-cost, modular, and open-source camera system          */
/*    for subsea observations", IEEE Oceans 2014                            */
/*                                                                          */
/****************************************************************************/

#include "sys_defs.h"

void spiInit()
{
    /* setup SPI1 registers */
    SPI1CON1bits.CKP = 1;
    SPI1CON1bits.CKE = 0;
    SPI1CON1bits.SMP = 0;
    
    SPI1CON1bits.MSTEN = 1;

    /* set primary and secondary prescalar */
    SPI1CON1bits.PPRE = 0x03;   /* pimary prescale 1:1      */
    SPI1CON1bits.SPRE = 0x06;   /* secondary prescale 2:1   */

    /* enable SPI module */
    SPI1STATbits.SPIEN = 1;

}

unsigned int spiReadWrite(unsigned int data)
{
    /* put data in SPI TX buffer */
    SPI1BUF = data;

    /* wait for xmit to finish */
    while ( !SPI1STATbits.SPIRBF )
        ; /* wait here */

    return SPI1BUF;
}

