/****************************************************************************/
/* Created 2011 (Anno Domini)                                               */
/****************************************************************************/
/* MBARI SeeStar (See http://www.mbari.org/seestar/) is licensed under a    */
/* Creative Commons Attribution-ShareAlike 4.0 International License.       */
/*                                                                          */
/* You should have received a copy of the license along with this work. If  */
/* not, see <http://creativecommons.org/licenses/by-sa/4.0/>.               */
/*                                                                          */
/* Citation: F. Cazenave, C. Kecy, M. Risi, S.H.D. Haddock (in press)       */
/*    "SeeStar: a low-cost, modular, and open-source camera system          */
/*    for subsea observations", IEEE Oceans 2014                            */
/*                                                                          */
/****************************************************************************/

#ifndef TASKS_H
#define TASKS_H

void tskInit();

void tskDebugOn();
void tskDebugOff();
int tskDebugState();

void tskExecute();

void tskSampleNow();
void tskEnableSampling();
void tskDisableSampling();

/* configuration settings */
unsigned int tskGetAutoRunMode();
int tskSetAutoRunMode(unsigned int mode);

unsigned long tskGetAutoRunDelay();
int tskSetAutoRunDelay(unsigned long val);

unsigned long tskGetAutoRunDuration();
int tskSetAutoRunDuration(unsigned long val);

unsigned int tskGetSampleMode();
int tskSetSampleMode(unsigned int mode);

unsigned long tskGetSampleInterval();
int tskSetSampleInterval(unsigned long val);

unsigned long tskGetSampleDuration();
int tskSetSampleDuration(unsigned long val);

unsigned long tskGetLedOnDelay();
int tskSetLedOnDelay(unsigned long val);

unsigned long tskGetLedOnDuration();
int tskSetLedOnDuration(unsigned long val);

unsigned long tskGetVoltStopLevel();
int tskSetVoltStopLevel(unsigned long val);

#endif
