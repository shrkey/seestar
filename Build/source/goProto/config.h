/****************************************************************************/
/* Created 2012 (Anno Domini)                                               */
/****************************************************************************/
/* MBARI SeeStar (See http://www.mbari.org/seestar/) is licensed under a    */
/* Creative Commons Attribution-ShareAlike 4.0 International License.       */
/*                                                                          */
/* You should have received a copy of the license along with this work. If  */
/* not, see <http://creativecommons.org/licenses/by-sa/4.0/>.               */
/*                                                                          */
/* Citation: F. Cazenave, C. Kecy, M. Risi, S.H.D. Haddock (in press)       */
/*    "SeeStar: a low-cost, modular, and open-source camera system          */
/*    for subsea observations", IEEE Oceans 2014                            */
/*                                                                          */
/****************************************************************************/

#ifndef CONFIG_H
#define CONFIG_H

typedef struct
{
    unsigned int bootFlag;
    unsigned int autoRunMode;
    unsigned long autoRunDelay;
    unsigned long autoRunDuration;
    unsigned int sampleMode;
    unsigned long sampleInterval;
    unsigned long sampleDuration;
    unsigned long ledOnDelay;
    unsigned long ledOnDuration;
    unsigned long voltStopLevel;
    unsigned long debugBits;

} ConfigData;

int cfgDefault();
int cfgCheck(ConfigData* config);
int cfgRead(ConfigData* config);
int cfgWrite(ConfigData* config);
int cfgSetBootFlag();

#endif

