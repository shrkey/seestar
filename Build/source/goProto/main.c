/****************************************************************************/
/* Created 2009 (Anno Domini)                                               */
/****************************************************************************/
/* MBARI SeeStar (See http://www.mbari.org/seestar/) is licensed under a    */
/* Creative Commons Attribution-ShareAlike 4.0 International License.       */
/*                                                                          */
/* You should have received a copy of the license along with this work. If  */
/* not, see <http://creativecommons.org/licenses/by-sa/4.0/>.               */
/*                                                                          */
/* Citation: F. Cazenave, C. Kecy, M. Risi, S.H.D. Haddock (in press)       */
/*    "SeeStar: a low-cost, modular, and open-source camera system          */
/*    for subsea observations", IEEE Oceans 2014                            */
/*                                                                          */
/****************************************************************************/

#include <p24fxxxx.h>
#include <stdio.h>
#include <string.h>
#include "dpslp.h"

#include "parser.h"
#include "errors.h"

#include "i2c.h"
#include "spi.h"
#include "config.h"
#include "analog.h"
#include "hero_io.h"
#include "ex_rtc.h"
#include "data_flash.h"
#include "serial.h"
#include "tasks.h"
#include "sys_timer.h"

#include "sys_defs.h"

/* Watchdog time disabled & Emulator pins are PGEC1/PGED1 & 
Write to program memory on & code protection off & jtag off */
_CONFIG1(FWDTEN_OFF & ICS_PGx1 & GWRP_OFF & GCP_OFF & JTAGEN_OFF)

/* XT osc mode & OSC functions as oscillator & Primary Oscillator XT*/
_CONFIG2(POSCMOD_XT & OSCIOFNC_OFF & FNOSC_PRI)

/* Use SOSC pins for IO & segmented code disabled  */
_CONFIG3(SOSCSEL_IO & WPDIS_WPDIS)

/* Brown out always enabled & Disable WDT */
_CONFIG4(DSBOREN_ON & DSWDTEN_OFF)


#define MAX_CHARS   80  /* maximum command line entry length */

static ConfigData configData;
extern int enableCounter;

int getCommand(char* cmd);

int main(void)
{
    int i = 0;
    int ret_val;
    char cmd_buff[MAX_CHARS];
    char err_buff[64];
   
    char* cmd_ptr;

    /* make sure analog channels are in digital mode */
    _PCFG0 = 1;     /* cam on/off pin   */
    _PCFG4 = 0;     /* Bus Current      */
    _PCFG5 = 0;     /* Bus Voltage      */
    _PCFG6 = 1;     /* GP Trig          */
    _PCFG7 = 1;     /* GP ID 1          */
    _PCFG8 = 1;     /* GP ID 2          */
    _PCFG9 = 1;     /* serial RX pin    */
    _PCFG10 = 1;    /* serial TX pin    */
    _PCFG11 = 1;    /* SPI CS pin       */
    _PCFG12 = 1;    /* SPI SCK pin      */

    /* setup outputs */
    _TRISA0 = 0;    /* CAM on/off       */
    _TRISB4 = 0;    /* blinky LED       */
    
    _LATA8 = 1;     /* DataFlash WP     */
    Nop();
    _TRISA8 = 0;

    _LATA9 = 1;     /* DataFlash HOLD   */
    Nop();
    _TRISA9 = 0;    
            
    _TRISC0 = 0;    /* GP Trig          */
    _TRISC2 = 0;    /* GP ID 2          */
    _TRISC3 = 0;    /* GP ID 3          */
    _TRISC5 = 0;    /* GP Mode          */

    _TRISC7 = 0;    /* LED Ctrl         */

    /* Map UARTs to pins */
    _U1RXR = 14; /* Make pin RP14 U1RX */
    _RP15R = 3;  /* Make pin RP15 U1TX */

    /* map SPI bus pins */
    _SDI1R = 10;    /* Make Pin RP10 SDI (MISO)  */
    _RP11R = 7;     /* Make Pin RP11 SDO (MOSI)  */
    _RP12R = 8;     /* Make Pin RP12 SCK         */
    _TRISB13 = 0;   /* Make Pin RB13 CS RTC      */
    _TRISC9 = 0;    /* Make Pin RC9 CS DataFlash */

    /* init stuff here */
    anaInit();
    spiInit(); 
    serInit();
    tmrInit();
    i2cInit();
    rtcInit();
    dmInit();
    hroInit();
    tskInit();
    
    /* show the sign-on banner */
    sprintf(cmd_buff, "\r\nSeeStar goProto Board %s, %s\r\n", __DATE__, __TIME__);
    serPutString(SER_CONSOLE, cmd_buff);

    sprintf(cmd_buff, "CVS tag \"%s\"\r\n\r\n", "$Name:  $");
    serPutString(SER_CONSOLE, cmd_buff);

    /* read the config struct */
    if ( cfgRead(&configData) != 0 )
    {
        /* set the defaults */
        cfgDefault();
        serPutString(SER_CONSOLE, "ERR: CONFIG READ FAILED, USING DEFAULT\r\n");

        /* reread the correct default config */
        cfgRead(&configData);
    }

    /* set all the config params */
    tskSetAutoRunMode(configData.autoRunMode);
    tskSetAutoRunDelay(configData.autoRunDelay);
    tskSetAutoRunDuration(configData.autoRunDuration);
    tskSetSampleMode(configData.sampleMode);
    tskSetSampleInterval(configData.sampleInterval);
    tskSetSampleDuration(configData.sampleDuration);
    tskSetLedOnDelay(configData.ledOnDelay);
    tskSetLedOnDuration(configData.ledOnDuration);
    tskSetVoltStopLevel(configData.voltStopLevel);

    /* set debugging for modules here */
    if (configData.debugBits & TSK_DEBUG_BIT) 
        tskDebugOn();

    if (configData.debugBits & HRO_DEBUG_BIT) 
        hroDebugOn();

    /* main loop */
    for(;;)
    {
        /* house keeping for timer module */
        tmrTask();
        
        /* task loop */
        tskExecute();

        if ( ++i == 4000 )
        {
            i = 0;
#if 0
            /* togle B4 */
            if ( _LATB4 )
                _LATB4 = 0;
            else
                _LATB4 = 1;
#endif
        }
        /* get command */
        if ( !getCommand(cmd_buff) )
        {
            ret_val = ERR_NO_LINE;
        }
        else
        {
            /* Skip white space here */
            cmd_ptr = prsSkip(cmd_buff, " \t");

            /* If the cmd from getCommand was empty then return here. */
            if ( !strlen(cmd_ptr) )
                ret_val = ERR_EMPTY_LINE;
            else
                ret_val = prsParse(cmd_ptr, cmdsRoot, ERR_NO_COMMAND);
        }

        /* display ERR code or RDY prompt */
        if ( ret_val != ERR_NO_LINE)
        {
            if ((ret_val != ERR_SUCCESS) && (ret_val != ERR_EMPTY_LINE))
            {
                sprintf(err_buff,"ERR %04d\r\n", ret_val);
                serPutString(SER_CONSOLE, err_buff);
            }

            serPutString(SER_CONSOLE, "RDY\r\n");
        }
            
    }

    return 0;
}


#define ECHO_MODE   1
int getCommand(char* cmd)
{
    static int char_count = 0;
    static char line[MAX_CHARS] = "";
    const unsigned char terminator = '\r';
    int got_line;
    unsigned char b;
    int i;
    
    got_line = FALSE;

    if ( serGetByte(1, &b) )
    {
        if ( ECHO_MODE )
        {
            serPutByte(1, b);
            if (b == '\r') serPutByte(1, '\n');
        }

        if ( b != terminator )
        {
            if ( (b != '\b') && (char_count < MAX_CHARS) )
                line[char_count++] = b;
            else if ( char_count > 0 )
                --char_count;

            cmd[0] = '\0';
        } 
        else
        {
            got_line = TRUE;
            line[char_count] = '\0';
            for (i = 0; i <= char_count; i++)
                cmd[i] = line[i];
            char_count = 0;
        }
    }

    return got_line;
}


