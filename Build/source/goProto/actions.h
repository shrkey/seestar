/****************************************************************************/
/* Created 2012 (Anno Domini)                                               */
/****************************************************************************/
/* MBARI SeeStar (See http://www.mbari.org/seestar/) is licensed under a    */
/* Creative Commons Attribution-ShareAlike 4.0 International License.       */
/*                                                                          */
/* You should have received a copy of the license along with this work. If  */
/* not, see <http://creativecommons.org/licenses/by-sa/4.0/>.               */
/*                                                                          */
/* Citation: F. Cazenave, C. Kecy, M. Risi, S.H.D. Haddock (in press)       */
/*    "SeeStar: a low-cost, modular, and open-source camera system          */
/*    for subsea observations", IEEE Oceans 2014                            */
/*                                                                          */
/****************************************************************************/

#ifndef ACTIONS_H
#define ACTIONS_H

/* Root commands */
int actGetCmd(char* s);
int actSetCmd(char* s);
int actClrCmd(char* s);

int actEEProgCmd(char* s);
int actEEReadCmd(char* s);

int actCamOnCmd(char* s);
int actCamOffCmd(char* s);
int actSnapHiCmd(char* s);
int actSnapLoCmd(char* s);
int actVidOnCmd(char* s);
int actVidOffCmd(char* s);

int actLedOnCmd(char* s);
int actLedOffCmd(char* s);

int actPwrOnCmd(char* s);
int actPwrOffCmd(char* s);

int actTimeCmd(char* s);

int actHelpCmd(char* s);
int actResetCmd(char* s);

int actSaveCmd(char* s);
int actParamCmd(char* s);
int actDefaultCmd(char* s);
int actSampleNowCmd(char* s);
int actUptimeCmd(char* s);

int actMemEraseCmd(char* s);
int actMemWriteCmd(char* s);
int actMemReadCmd(char* s);

/* debug command(s) below */                
int actTest1Cmd(char* s);
int actTest2Cmd(char* s);
int actTest3Cmd(char* s);
int actTest4Cmd(char* s);

/* Get commands */
int actGetLedCmd(char* s);
int actGetDebugCmd(char* s);
int actGetPowerCmd(char* s);

/* Set commands */
int actSetTimeCmd(char* s);
int actSetDebugCmd(char* s);

int actSetAutoRunModeCmd(char* s);
int actSetAutoRunDelayCmd(char* s);
int actSetAutoRunDurationCmd(char* s);
int actSetSampleModeCmd(char* s);
int actSetSampleIntervalCmd(char* s);
int actSetSampleDurationCmd(char* s);
int actSetLedOnDelayCmd(char* s);
int actSetLedOffDelayCmd(char* s);
int actSetVoltStopLevelCmd(char* s);

/* Clear commands */
int actClrDebugCmd(char* s);

#endif
