/****************************************************************************/
/* Created 2012 (Anno Domini)                                               */
/****************************************************************************/
/* MBARI SeeStar (See http://www.mbari.org/seestar/) is licensed under a    */
/* Creative Commons Attribution-ShareAlike 4.0 International License.       */
/*                                                                          */
/* You should have received a copy of the license along with this work. If  */
/* not, see <http://creativecommons.org/licenses/by-sa/4.0/>.               */
/*                                                                          */
/* Citation: F. Cazenave, C. Kecy, M. Risi, S.H.D. Haddock (in press)       */
/*    "SeeStar: a low-cost, modular, and open-source camera system          */
/*    for subsea observations", IEEE Oceans 2014                            */
/*                                                                          */
/****************************************************************************/
/* Routines derived from Glen Akins gptestbrd01.c program                   */
/* (See http://bikerglen.com/ )                                             */
/****************************************************************************/

#ifndef I2C_H
#define I2C_H

#include <p24fxxxx.h>

/* bit-banged i2c interface */
#define SCL             _LATB8
#define WRITE_scl()     _TRISB8 = 0
#define SDA             _LATB9
#define SDARD           _RB9
#define READ_sda()      _TRISB9 = 1
#define WRITE_sda()     _TRISB9 = 0

void i2cInit();
void i2cAckPolling(unsigned char device_address);
void i2cStart(void);
void i2cStop(void);
unsigned char i2cReadByte(void);
unsigned char i2cSendByte(unsigned char out_byte);
void i2cDelay(unsigned char loops);

#endif
