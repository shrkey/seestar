#SeeStar Open Source Documentation List

1. Operations Manual
2. Technical Manual
3. Software Manual


###1. Operations Manual
This document will be a comprehensive guide to operating a SeeStar system. This is intended to be used by user with a pre-assembled SeeStar who needs to get a system deployed.

Sections/topics (order TBD):

* System overview (simple description)
* System capabilities (image & video options)
* Programming the SeeStar system (basic commands)
* GoPro Scripts
* GoPro Camera setup
* Electrical connections
* USB image/video transfer



###2. Technical Manual
This document will cover the assembly instructions and theory of operation of the individual modules. This manual will contain all of the information necessary to build a complete SeeStar system.

Sections/topics (order TBD):

* ####Camera module
    - Electrical/mechanical specifications
    - Mechanical build
    - Bill of materials
    - Controller board
	- Theory of operation
	- Schematic
	- Bill of materials

- ####LED module
       - Electrical/mechanical specifications
       - Mechanical build
       - Bill of materials
       - LED board
		- Theory of operation
		- Schematic
		- Bill of materials
       - Driver board
		- Theory of operation
		- Schematic
		- Bill of materials


* ####Battery Module (NiMH)
     - Electrical/mechanical specifications
     - Mechanical build
     - Bill of materials
     - Electrical schematic


###3. Software Manual
This document will go into more detail regarding the programming and firmware of the SeeStar system. The intended audience is the more advanced user who wishes to modify their system to suit their specific needs.

Sections/topics (order TBD):

* Programming the Controller board (PIC24)
* Firmware description
* EEPROM code description (?)
* Programming the SeeStar system (extended commands)
* Flash memory data description








     	








